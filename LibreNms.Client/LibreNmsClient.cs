﻿using LibreNms.Client.Helpers;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace LibreNms.Client
{
    public class LibreNmsClient
    {
        public string ApiUrl { get; set; }
        public string ApiToken { get; set; }

        public LibreNmsClient(string apiUrl, string apiToken)
        {
            ApiUrl = apiUrl;
            ApiToken = apiToken;
        }

        public async Task<Models.Devices.Get.ResponseModel> GetDeviceDetailsByHostnameAsync(string hostname)
        {
            return JsonConvert.DeserializeObject<Models.Devices.Get.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/devices/" + hostname, ApiToken));
        }

        public async Task<Models.Devices.Get.ResponseModel> GetDeviceDetailsByIdAsync(string id)
        {
            return JsonConvert.DeserializeObject<Models.Devices.Get.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/devices/" + id, ApiToken));
        }

        public async Task<Models.Services.Get.ResponseModel> GetServicesByHostnameAsync(string hostname)
        {
            return JsonConvert.DeserializeObject<Models.Services.Get.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/services/" + hostname, ApiToken));
        }

        public async Task<Models.Services.Get.ResponseModel> GetServicesByIdAsync(string id)
        {
            return JsonConvert.DeserializeObject<Models.Services.Get.ResponseModel>(await HttpHelper.GetAsync(ApiUrl + "/services/" + id, ApiToken));
        }
    }
}
