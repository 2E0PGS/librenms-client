﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreNms.Client.Models.Devices.Get
{
    public class ResponseModel
    {
        public string Status { get; set; }
        public List<DeviceModel> Devices { get; set; }
    }
}
