﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LibreNms.Client.Models.Devices.Get
{
    public class DeviceModel
    {
        [JsonProperty(PropertyName = "device_id")]
        public string DeviceId { get; set; }
        public string Hostname { get; set; }
        public string SysName { get; set; }
        public string SysDescr { get; set; }
        public string SysContact { get; set; }
        public string Version { get; set; }
        public string Hardware { get; set; }
        public string OS { get; set; }
        public int Status { get; set; }
        public int? Uptime { get; set; }
        [JsonProperty(PropertyName = "last_polled")]
        public DateTime? LastPolled { get; set; }
        public string Purpose { get; set; }
        public string Location { get; set; }
        [JsonProperty(PropertyName = "os_group")]
        public string OSGroup { get; set; }
    }
}
