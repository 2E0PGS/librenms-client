using System;
using System.Collections.Generic;
using System.Text;

namespace LibreNms.Client.Models.Services.Get
{
    public class ResponseModel
    {
        public string Status { get; set; }
        public List<List<ServiceModel>> Services { get; set; }
    }
}
