using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LibreNms.Client.Models.Services.Get
{
    public class ServiceModel
    {
        [JsonProperty(PropertyName = "service_id")]
        public int ServiceId { get; set; }

        [JsonProperty(PropertyName = "device_id")]
        public int DeviceId { get; set; }

        [JsonProperty(PropertyName = "service_ip")]
        public string ServiceIp { get; set; }

        [JsonProperty(PropertyName = "service_type")]
        public string ServiceType { get; set; }

        [JsonProperty(PropertyName = "service_desc")]
        public string ServiceDesc { get; set; }

        [JsonProperty(PropertyName = "service_param")]
        public string ServiceParam { get; set; }

        //[JsonProperty(PropertyName = "service_ignore")]
        //public bool ServiceIgnore { get; set; }

        [JsonProperty(PropertyName = "service_status")]
        public int ServiceStatus { get; set; }

        [JsonProperty(PropertyName = "service_changed")]
        public int ServiceChanged { get; set; }

        [JsonProperty(PropertyName = "service_message")]
        public string ServiceMessage { get; set; }

        //[JsonProperty(PropertyName = "service_disabled")]
        //public bool ServiceDisabled { get; set; }

        [JsonProperty(PropertyName = "service_ds")]
        public string ServiceDS { get; set; }
    }
}