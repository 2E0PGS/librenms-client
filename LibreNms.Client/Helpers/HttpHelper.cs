﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LibreNms.Client.Helpers
{
    internal static class HttpHelper
    {
        public static async Task<string> GetAsync(string apiUrl, string apiToken)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("X-Auth-Token", apiToken);
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(apiUrl);
            string content = await httpResponseMessage.Content.ReadAsStringAsync();
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                return content;
            }
            else
            {
                throw new HttpRequestException(content);
            }
        }
    }
}
